import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
import scrapy.http.request
import re
import lxml.html


class Section:
    def __init__(self, title: str, content):
        self.title = title
        self.content = content


class Article:
    def __init__(self, url: str, sections: [Section]):
        self.url = url
        self.title = None  # extract title
        self.sections = sections


articles: [Article]
articles = []


def extract_text(htmltext):
    return lxml.html.fromstring(htmltext).text_content()


def extract_links(block, raw, plaintext):
    all_links = block.css('a').getall()
    # filter by wikipedia links
    return all_links
    # return filter(lambda link: link.startswith('/wiki/'), block.css('a::attr(href)').getall())


class WikiSpider(scrapy.Spider):

    name = 'wiki'

    allowed_domains = ['en.wikipedia.org']

    start_urls = [
        'https://en.wikipedia.org/wiki/Circle_of_fifths',
    ]

    """
    rules = (
        Rule(
            LinkExtractor(
                allow=('/wiki/', ),
                deny=('Wikipedia:Media_help',),
                restrict_css='.mw-parser-output > *',
                unique=True),
            callback='parse'
        ),
    )
    """

    link_extractor = LinkExtractor(
        allow=('/wiki/',),
        deny=(
            'Wikipedia:Media_help',
        ),
        restrict_css='.mw-parser-output > *',
        unique=True
    )


    def parse(self, response: scrapy.http.TextResponse):

        url = response.url

        # self.logger.info("Parsing: %s", url)

        # the page dictionary will be returned, it contains all parsed data about this article
        page = {
            'url': url,
            'title': response.css('.firstHeading::text').get(),
            'body': [],
        }

        # analyze each block of the content and add it to the page dictionary
        for block in response.css('.mw-parser-output > h2, p'):

            # the raw html of that block for debugging/verification purposes
            raw = block.get()

            # headline/paragraph
            if raw[1] == 'p':
                blocktype = 'paragraph'
            elif raw[1] == 'h':
                blocktype = 'headline'
            else:
                blocktype = 'unknown'

            text = extract_text(raw)

            links = WikiSpider.link_extractor.extract_links(response)

            yield from response.follow_all(links, callback=self.parse)

            links = list(map(lambda x: x.url, links))

            textblock = {
                'type': blocktype,
                'text': text,
                'links': links
            }
            page['body'].append(textblock)

        yield page

